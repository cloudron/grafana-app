FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-releases depName=grafana/grafana versioning=semver extractVersion=^v(?<version>.+)$
ARG GRAFANA_VERSION=11.5.2

RUN curl -L https://dl.grafana.com/oss/release/grafana-${GRAFANA_VERSION}.linux-amd64.tar.gz | tar zxvf - -C /app/code --strip-components 1

COPY custom.ini.template start.sh /app/pkg/

# this is used by the CLI tool. it does not read this path from the config file for some reason
ENV GF_PLUGIN_DIR /app/data/plugins

CMD [ "/app/pkg/start.sh" ]
