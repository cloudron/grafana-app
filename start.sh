#!/bin/bash

set -eu

mkdir -p /app/data/plugins /app/data/grafana /run/grafana/logs

if [[ ! -f /app/data/custom.ini ]]; then
    echo -e "; Add customizations here - https://grafana.com/docs/grafana/latest/administration/configuration/\n" > /app/data/custom.ini
    crudini --set "/app/data/custom.ini" security secret_key $(pwgen -1 -s)
fi

[[ -z "${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-}" ]] && export CLOUDRON_MAIL_FROM_DISPLAY_NAME="Grafana"
[[ -z "${CLOUDRON_OIDC_PROVIDER_NAME:-}" ]] && export CLOUDRON_OIDC_PROVIDER_NAME="Cloudron"

# merge user config file
cp /app/pkg/custom.ini.template "/run/grafana/custom.ini"
crudini --merge "/run/grafana/custom.ini" < "/app/data/custom.ini"
if [[ -z "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    crudini --set "/run/grafana/custom.ini" auth.generic_oauth enabled false
fi

echo "=> Setting permissions"
chown -R cloudron:cloudron /app/data /run/grafana

echo "=> Starting Grafana"
exec /usr/local/bin/gosu cloudron:cloudron /app/code/bin/grafana-server -homepath /app/code -packaging cloudron -config /run/grafana/custom.ini
