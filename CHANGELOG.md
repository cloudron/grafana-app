[0.1.0]
* Initial version

[0.2.0]
* Add tests
* Add documentation url

[0.3.0]
* Add note on how to change password

[1.0.0]
* Set GF_PLUGIN_DIR env var for the CLI tool

[1.1.0]
* Update Grafana to 7.2.2
* [Full changelog](https://github.com/grafana/grafana/releases/tag/v7.2.0)
* Alerting: Ensuring notifications displayed correctly in mobile device with Google Chat. #27578, @alvarolmedo
* TraceView: Show full traceID and better discern multiple stackTraces in span details. #27710, @aocenas
* DataLinks: Fixes issue with data links not interpolating values with correct field config. #27622, @torkelo
* DataProxy: Ignore empty URL's in plugin routes. #27653, @domasx2
* Stat panel: Fix problem where string values where always green. #27656, @peterholmberg

[1.2.0]
* Update Grafana to 7.3.5
* [Full changelog](https://github.com/grafana/grafana/releases/tag/v7.3.0)
* AzureMonitor: Support decimal (as float64) type in analytics/logs. #28480, @kylebrandt
* Plugins signing: UI information. #28469, @dprokop
* Short URL: Update last seen at when visiting a short URL. #28565, @marefr

[1.2.1]
* Update Grafana to 7.3.7
* [Full changelog](https://github.com/grafana/grafana/releases/tag/v7.3.7)
* Auth: Add missing request headers to SigV4 middleware allowlist. #30115, @wbrowne
* SeriesToRows: Fixes issue in transform so that value field is always named Value. #30054, @torkelo

[1.3.0]
* Use latest base image 3.0.0

[1.3.1]
* Update Grafana to 7.4.1
* [Full changelog](https://github.com/grafana/grafana/releases/tag/v7.4.1)
* Make value mappings correctly interpret numeric-like strings. #30893, @dprokop
* Variables: Adds queryparam formatting option. #30858, @hugohaggmark

[1.3.2]
* Update Grafana to 7.4.2
* [Full changelog](https://github.com/grafana/grafana/releases/tag/v7.4.2)
* Explore: Do not show non queryable data sources in data source picker. #31144, @torkelo
* Snapshots: Disallow anonymous user to create snapshots. #31263, @marefr

[1.3.3]
* Update Grafana to 7.4.3
* [Full changelog](https://github.com/grafana/grafana/releases/tag/v7.4.3)
* AdHocVariables: Fix an issue where adhoc filter values stored as integer was causing Grafana to crash. #31382, @hugohaggmark
* DashboardLinks: Fix an issue where the dashboard links were causing a full page reload. #31334, @torkelo
* Elasticsearch: Fix query initialization logic & query transformation from Prometheus/Loki. #31322, @Elfo404
* QueryEditor: Fix disabling queries in dashboards. #31336, @gabor
* Streaming: Fix an issue with the time series panel and streaming data source when scrolling back from being out of view. #31431, @torkelo

[1.3.4]
* Update Grafana to 7.4.5
* [Full changelog](https://github.com/grafana/grafana/releases/tag/v7.4.5)

[1.4.0]
* Update Grafana to 7.5.0
* [Full changelog](https://github.com/grafana/grafana/releases/tag/v7.5.0)
* Alerting: Add ability to include aliases with hyphen in InfluxDB. #32262, @grafanabot
* CloudWatch: Use latest version of aws sdk. #32217, @sunker

[1.4.1]
* Update Grafana to 7.5.1
* [Full changelog](https://github.com/grafana/grafana/releases/tag/v7.5.1)

[1.4.2]
* Update Grafana to 7.5.2
* [Full changelog](https://github.com/grafana/grafana/releases/tag/v7.5.2)
* Explore: Set Explore's GraphNG to use default value for connected null values setting

[1.4.3]
* Update Grafana to 7.5.3
* [Full changelog](https://github.com/grafana/grafana/releases/tag/v7.5.3)
* Dashboard: Do not include default datasource when externally exporting dashboard with row. #32494, @kaydelaney
* Loki: Remove empty annotations tags. #32359, @conorevans

[1.4.4]
* Update Grafana to 7.5.4
* [Full changelog](https://github.com/grafana/grafana/releases/tag/v7.5.4)
* AzureMonitor: Add support for Microsoft.AppConfiguration/configurationStores namespace. #32123, @deesejohn
* TablePanel: Make sorting case-insensitive. #32435, @kaydelaney

[1.4.5]
* Update Grafana to 7.5.5
* [Full changelog](https://github.com/grafana/grafana/releases/tag/v7.5.5)
* Explore: Load default data source in Explore when the provided source does not exist. #32992, @ifrost
* Instrumentation: Add success rate metrics for email notifications. #33359, @bergquist

[1.4.6]
* Update Grafana to 7.5.6
* Database: Add isolation level configuration parameter for MySQL
* Instrumentation: Don't consider invalid email address a failed email.
* InfluxDB: Improve measurement-autocomplete behavior.
* Loki: fix label browser crashing when + typed.
* Prometheus: Sanitize PromLink button.

[1.4.7]
* Update Grafana to 7.5.7
* Updated relref to "Configuring exemplars" section (#34240) (#34243)

[1.5.0]
* Update Grafana to 8.0.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.0.0)

[1.5.1]
* Update Grafana to 8.0.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.0.1)

[1.5.2]
* Update Grafana to 8.0.2
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.0.2)

[1.5.3]
* Update Grafana to 8.0.3
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.0.3)

[1.5.4]
* Update Grafana to 8.0.4
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.0.4)

[1.5.5]
* Update Grafana to 8.0.5
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.0.5)
* Cloudwatch Logs: Send error down to client. #36277, @zoltanbedi
* Folders: Return 409 Conflict status when folder already exists. #36429, @dsotirakis
* TimeSeries: Do not show series in tooltip if it's hidden in the viz. #36353, @dprokop

[1.5.6]
* Update Grafana to 8.0.6
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.0.6)

[1.6.0]
* Update Grafana to 8.1.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.1.0)
* Alerting: Deduplicate receivers during migration. #36812, @codesome
* ColorPicker: Display colors as RGBA. #37231, @nikki-kiga
* Select: Make portalling the menu opt-in, but opt-in everywhere. #37501, @ashharrison90
* TimeRangePicker: Improve accessibility. #36912, @tskarhed

[1.6.1]
* Update Grafana to 8.1.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.1.1)

[1.6.2]
* Update Grafana to 8.1.2
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.1.2)

[1.6.3]
* Update Grafana to 8.1.3
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.1.3)

[1.6.4]
* Update Grafana to 8.1.4
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.1.4)

[1.6.5]
* Update Grafana to 8.1.5
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.1.5)

[1.6.6]
* Update Grafana to 8.1.6
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.1.6)

[1.6.7]
* Update Grafana to 8.1.7
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.1.7)

[1.7.0]
* Update Grafana to 8.2.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.2.0)

[1.7.1]
* Update Grafana to 8.2.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.2.1)

[1.7.2]
* Update Grafana to 8.2.2
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.2.2)

[1.7.3]
* Update Grafana to 8.2.3
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.2.3)

[1.7.4]
* Update Grafana to 8.2.4
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.2.4)

[1.8.0]
* Fix email sending
* Support for Optional SSO

[1.8.1]
* Update Grafana to 8.2.5
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.2.5)

[1.9.0]
* Update Grafana to 8.3.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.3.0)

[1.9.1]
* Update Grafana to 8.3.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.3.1)
* Security: Fixes CVE-2021-43798

[1.9.2]
* Update Grafana to 8.3.2
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.3.2)
* Security: Fixes CVE-2021-43813 and CVE-2021-PENDING.

[1.9.3]
* Update Grafana to 8.3.3
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.3.3)
* BarChart: Use new data error view component to show actions in panel edit. #42474, @torkelo
* CloudMonitor: Iterate over pageToken for resources. #42546, @iwysiu
* Macaron: Prevent WriteHeader invalid HTTP status code panic. #42973, @bergquist

[1.9.4]
* Update Grafana to 8.3.4
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.3.4)
* Alerting: Allow configuration of non-ready alertmanagers. #43063, @alexweav
* Alerting: Allow customization of Google chat message. #43568, @alexweav
* Alerting: Allow customization of Google chat message (#43568). #43723, @alexweav
* AppPlugins: Support app plugins with only default nav. #43016, @torkelo
* InfluxDB: InfluxQL: query editor: skip fields in metadata queries. #42543, @gabor
* Prometheus: Forward oauth tokens after prometheus datasource migration. #43686, @MasslessParticle

[1.9.5]
* Update Grafana to 8.3.6
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.3.6)
* Cloud Monitoring: Reduce request size when listing labels. #44365, @mtanda
* Explore: Show scalar data result in a table instead of graph. #44362, @tharun208
* Snapshots: Updates the default external snapshot server URL. #44563, @DanCech
* Table: Makes footer not overlap table content. #44210, @dprokop
* Tempo: Add request histogram to service graph datalink. #44671, @connorlindsey
* Tempo: Add time range to tempo search query behind a feature flag. #43811, @connorlindsey
* Tempo: Auto-clear results when changing query type. #44390, @connorlindsey
* Tempo: Display start time in search results as relative time. #44568, @tharun208
* CloudMonitoring: Fix resource labels in query editor. #44550, @iwysiu
* Cursor sync: Apply the settings without saving the dashboard. #44270, @dprokop
* LibraryPanels: Fix for Error while cleaning library panels. #45033, @hugohaggmark
* Logs Panel: fix timestamp parsing for string dates without timezone. #44664, @Elfo404
* Prometheus: Fix some of the alerting queries that use reduce/math operation. #44380, @ivanahuckova
* TablePanel: Fix ad-hoc variables not working on default datasources. #44314, @joshhunt
* Text Panel: Fix alignment of elements. #44313, @ashharrison90
* Variables: Fix for constant variables in self referencing links. #44631, @hugohaggmark

[1.10.0]
* Update Grafana to 8.4.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.4.0)

[1.10.1]
* Update Grafana to 8.4.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.4.1)
* Cloudwatch: Add support for AWS/PrivateLink* metrics and dimensions. #45515, @szymonpk
* Configuration: Add ability to customize okta login button name and icon. #44079, @DanCech
* Tempo: Switch out Select with AsyncSelect component to get loading state in Tempo Search. #45110, @CatPerry
* Alerting: Fix migrations by making send_alerts_to field nullable. #45572, @santihernandezc

[1.10.2]
* Update Grafana to 8.4.2
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.4.2)

[1.10.3]
* Update Grafana to 8.4.3
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.4.3)
* Alerting: Grafana uses > instead of >= when checking the For duration. #46010
* Alerting: Use expanded labels in dashboard annotations. #45726, @grobinson-grafana
* Logs: Escape windows newline into single newline. #45771, @perosb
* Alerting: Fix use of > instead of >= when checking the For duration. #46011, @grobinson-grafana
* Azure Monitor: Fixes broken log queries that use workspace. #45820, @sunker
* CloudWatch: Remove error message when using multi-valued template vars in region field. #45886, @sunker
* Middleware: Fix IPv6 host parsing in CSRF check. #45911, @ying-jeanne

[1.10.4]
* Update Grafana to 8.4.4
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.4.4)

[1.10.5]
* Update Grafana to 8.4.5
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.4.5)
* Instrumentation: Make backend plugin metrics endpoints available with optional authentication. #46467, @marefr
* Table panel: Show datalinks for cell display modes JSON View and Gauge derivates. #46020, @mdvictor
* Azure Monitor: Small bug fixes for Resource Picker. #46665, @sarahzinger
* Logger: Use specified format for file logger. #46970, @sakjur
* Logs: Handle missing fields in dataframes better. #46963, @gabor
* ManageDashboards: Fix error when deleting all dashboards from folder view. #46877, @joshhunt

[1.10.6]
* Update Grafana to 8.4.6
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.4.6)

[1.10.7]
* Update Grafana to 8.4.7
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.4.7)

[1.11.0]
* Update Grafana to 8.5.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.5.0)

[1.11.1]
* Update Grafana to 8.5.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.5.1)
* Azure Monitor: Fix space character encoding for metrics query link to Azure Portal. #48139, @kevinwcyu
* CloudWatch: Prevent log groups from being removed on query change. #47994, @asimpson
* Cloudwatch: Fix template variables in variable queries. #48140, @iwysiu
* Explore: Prevent direct access to explore if disabled via feature toggle. #47714, @Elfo404
* InfluxDB: Fixes invalid no data alerts. #48295, @yesoreyeram

[1.11.2]
* Update Grafana to 8.5.2
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.5.2)
* Alerting: Add safeguard for migrations that might cause dataloss. #48526, @JohnnyQQQQ
* AzureMonitor: Add support for not equals and startsWith operators when creating Azure Metrics dimension filters. #48077, @aangelisc
* Elasticsearch: Add deprecation notice for < 7.10 versions. #48506, @ivanahuckova
* Traces: Filter by service/span name and operation in Tempo and Jaeger. #48209, @joey-grafana

[1.11.3]
* Update Grafana to 8.5.4
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.5.4)
* Alerting: Remove disabled flag for data source when migrating alerts. #48559, @yuri-tceretian
* Alerting: Show notification tab of legacy alerting only to editor. #49624, @yuri-tceretian
* Alerting: Update migration to migrate only alerts that belong to existing org\dashboard. #49192, @yuri-tceretian
* AzureMonitor: Do not quote variables when a custom "All" variable option is used. #49428, @andresmgot
* AzureMonitor: Update allowed namespaces. #48468, @jcolladokuri
* CloudMonitor: Correctly encode default project response. #49510, @aangelisc
* Cloudwatch: Add support for new AWS/RDS EBS* metrics. #48798, @szymonpk

[1.11.4]
* Update Grafana to 8.5.5
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.5.5)

[1.11.5]
* Update Grafana to 8.5.6
* [Changelog](https://github.com/grafana/grafana/releases/tag/v8.5.6)

[1.12.0]
* Update Grafana to 9.0.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.0.0)
* API: Add GET /api/annotations/:annotationId endpoint. #47739, @scottbock
* API: Add endpoint for updating a data source by its UID. #49396, @papagian
* AccessControl: Document basic roles changes and provisioning V2. #48910, @gamab
* AccessControl: Enable RBAC by default. #48813, @IevaVasiljeva

[1.12.1]
* Update Grafana to 9.0.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.0.1)
* Alerting: Add support for image annotation in Alertmanager alerts. #50686, @grobinson-grafana
* Alerting: Add support for images in SensuGo alerts. #50718, @grobinson-grafana
* Alerting: Add support for images in Threema alerts. #50734, @grobinson-grafana
* Alerting: Adds Mimir to Alertmanager data source implementation. #50943, @gillesdemey
* Alerting: Invalid setting of enabled for unified alerting should return error. #49876, @grobinson-grafana

[1.12.2]
* Update Grafana to 9.0.2
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.0.2)
* Alerting: Add support for images in Pushover alerts. #51372, @grobinson-grafana
* Alerting: Don't stop the migration when alert rule tags are invalid. #51253, @gotjosh
* Alerting: Don't stop the migration when alert rule tags are invalid (…. #51341, @gotjosh
* Alerting: Skip the default data source if incompatible. #51452, @gillesdemey
* AzureMonitor: Parse non-fatal errors for Logs. #51320, @andresmgot
* OAuth: Restore debug log behavior. #51244, @Jguer
* Plugins: Improved handling of symlinks. #51324, @marefr
* Alerting: Code-gen parsing of URL parameters and fix related bugs. #51353, @alexweav
* Alerting: Code-gen parsing of URL parameters and fix related bugs. #50731, @alexweav
* Annotations: Fix annotation autocomplete causing panels to crash. #51164, @ashharrison90
* Barchart: Fix warning not showing. #51190, @joshhunt
* CloudWatch: Enable custom session duration in AWS plugin auth. #51322, @sunker
* Dashboards: Fixes issue with the initial panel layout counting as an unsaved change. #51315, @JoaoSilvaGrafana
* Plugins: Use a Grafana specific SDK logger implementation for core plugins. #51229, @marefr
* Search: Fix pagination in the new search page. #51366, @ArturWierzbicki

[1.12.3]
* Update Grafana to 9.0.3
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.0.3)
* Access Control: Allow dashboard admins to query org users. #51652, @IevaVasiljeva
* Access control: Allow organisation admins to add existing users to org. #51668, @IevaVasiljeva
* Alerting: Add method to provisioning API for obtaining a group and its rules. #51761, @alexweav
* Alerting: Add method to provisioning API for obtaining a group and its rules. #51398, @alexweav
* Alerting: Allow filtering of contact points by name. #51933, @alexweav
* Alerting: Disable /api/admin/pause-all-alerts with Unified Alerting. #51895, @joeblubaugh
* Analytics: Add total queries and cached queries in usage insights logs. (Enterprise)

[1.12.4]
* Update Grafana to 9.0.4
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.0.4)
* Browse/Search: Make browser back work properly when visiting Browse or search. #52271, @torkelo
* Logs: Improve getLogRowContext API. #52130, @gabor
* Loki: Improve handling of empty responses. #52397, @gabor
* Plugins: Always validate root URL if specified in signature manfiest. #52332, @wbrowne
* Preferences: Get home dashboard from teams. #52225, @sakjur
* SQLStore: Support Upserting multiple rows. #52228, @joeblubaugh
* Traces: Add more template variables in Tempo & Zipkin. #52306, @joey-grafana
* Traces: Remove serviceMap feature flag. #52375, @joey-grafana

[1.12.5]
* Update Grafana to 9.0.5
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.0.5)
* Access control: Show dashboard settings to users who can edit dashboard. #52535, @grafanabot
* Alerting: Allow the webhook notifier to support a custom Authorization header. #52515, @gotjosh
* Chore: Upgrade to Go version 1.17.12. #52523, @sakjur
* Plugins: Add signature wildcard globbing for dedicated private plugin type. #52163, @wbrowne
* Prometheus: Don't show errors from unsuccessful API checks like rules or exemplar checks. #52193, @darrenjaneczek
* Access control: Allow organisation admins to add existing users to org (#51668). #52553, @vtorosyan
* Alerting: Fix alert panel instance-based rules filtering. #52583, @konrad147
* Apps: Fixes navigation between different app plugin pages. #52571, @torkelo
* Cloudwatch: Upgrade grafana-aws-sdk to fix auth issue with secret keys. #52420, @sarahzinger
* Grafana/toolkit: Fix incorrect image and font generation for plugin builds. #52661, @academo
* Loki: Fix show context not working in some occasions. #52458, @svennergr
* RBAC: Fix permissions on dashboards and folders created by anonymous users. #52615, @gamab

[1.12.6]
* Update Grafana to 9.0.6
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.0.6)
* Access Control: Allow org admins to invite new users to their organization. #52904, @IevaVasiljeva
* Grafana/toolkit: Fix incorrect image and font generation for plugin builds. #52927, @academo
* Prometheus: Fix adding of multiple values for regex operator. #52978, @ivanahuckova
* UI/Card: Fix card items always having pointer cursor. #52809, @gillesdemey

[1.12.7]
* Update Grafana to 9.0.7
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.0.7)
* CloudMonitoring: Remove link setting for SLO queries. #53031, @andresmgot
* GrafanaUI: Render PageToolbar's leftItems regardless of title's presence. #53285, @Elfo404

[1.13.0]
* Update Grafana to 9.1.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.1.0)

[1.13.1]
* Update Grafana to 9.1.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.1.1)
* Cloud Monitoring: Support SLO burn rate. #53710, @itkq
* Schema: Restore "hidden" in LegendDisplayMode. #53925, @academo
* Timeseries: Revert the timezone(s) property name change back to singular. #53926, @academo
* Alerting: Fix links in Microsoft Teams notifications. #54003, @grobinson-grafana
* Alerting: Fix notifications for Microsoft Teams. #53810, @grobinson-grafana
* Alerting: Fix width of Adaptive Cards in Teams notifications. #53996, @grobinson-grafana
* ColorPickerInput: Fix popover in disabled state. #54000, @Clarity-89
* Decimals: Fixes auto decimals to behave the same for positive and negative values. #53960, @JoaoSilvaGrafana
* Loki: Fix unique log row id generation. #53932, @gabor
* Plugins: Fix file extension in development authentication guide. #53838, @pbzona
* TimeSeries: Fix jumping legend issue. #53671, @zoltanbedi
* TimeSeries: Fix memory leak on viz re-init caused by KeyboardPlugin. #53872, @leeoniya

[1.13.2]
* Update Grafana to 9.1.2
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.1.2)
* AdHoc variable: Correctly preselect datasource when provisioning. #54088, @dprokop
* AzureMonitor: Added ARG query function for template variables. #53059, @yaelleC
* Dashboard save: Persist details message when navigating through dashboard save drawer's tabs. #54084, @vbeskrovnov
* Dashboards: Correctly migrate mixed data source targets. #54152, @dprokop
* Elasticsearch: Use millisecond intervals for alerting. #54157, @gabor
* Elasticsearch: Use millisecond intervals in frontend. #54202, @gabor
* Geomap: Local color range. #54348, @adela-almasan
* Plugins Catalog: Use appSubUrl to generate plugins catalog urls. #54426, @academo
* Rendering: Add support for renderer token. #54425, @joanlopez

[1.13.3]
* Update Grafana to 9.1.3
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.1.3)

[1.13.4]
* Update Grafana to 9.1.4
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.1.4)
* GrafanaUI: Fixes Chrome issue for various query fields. #54566, @kaydelaney

[1.13.5]
* Update Grafana to 9.1.5
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.1.5)
* Alerting: Sanitize invalid label/annotation names for external alertmanagers. #54537, @JacobsonMT
* Alerting: Telegram: Truncate long messages to avoid send error. #54339, @ZloyDyadka
* DisplayProcessor: Handle reverse-ordered data when auto-showing millis. #54923, @leeoniya
* Heatmap: Add option to reverse color scheme. #54365, @leeoniya
* PluginLoader: Alias slate-react as @grafana/slate-react. #55027, @kaydelaney
* Search: Add substring matcher, to bring back the old dashboard search behavior. #54813, @ArturWierzbicki
* Traces: More visible span colors. #54513, @joey-grafana
* Alerting: Fix incorrect propagation of org ID and other fields in rule provisioning endpoints. #54603, @alexweav
* Alerting: Resetting the notification policy tree to the default policy will also restore default contact points. #54608, @alexweav
* AzureMonitor: Fix custom namespaces. #54937, @asimpson
* AzureMonitor: Fix issue where custom metric namespaces are not included in the metric namespace list. #54826, @andresmgot
* CloudWatch: Fix display name of metric and namespace. #54860, @sunker
* Cloudwatch: Fix annotation query serialization issue. #54884, @sunker
* Dashboard: Fix issue where unsaved changes warning would appear even after save, and not being able to change library panels. #54706, @torkelo
* Dashboard: Hide overflow content for single left pane. #54882, @lpskdl
* Loki: Fix a bug where adding adhoc filters was not possible. #54920, @svennergr
* Reports: Fix handling expired state. (Enterprise)

[1.13.6]
* Update Grafana to 9.1.6
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.1.6)
* DataSource: Adding possibility to hide queries from the inspector. #54892, @mckn
* Inspect: Hide Actions tab when it is empty. #55272, @ryantxu
* PanelMenu: Remove hide legend action as it was showing on all panel types. #54876, @torkelo
* Provisioning Contact points: Support disableResolveMessage via YAML. #54122, @mmusenbr
* PublicDashboards: Support subpaths when generating pubdash url. #55204, @owensmallwood

[1.13.7]
* Update Grafana to 9.1.7
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.1.7)

[1.14.0]
* Update Grafana to 9.2.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.2.0)
* Alerting: Add Notification error feedback on contact points view. #56225, @soniaAguilarPeiron
* Alerting: Allow created by to be manually set when there's no creator for silences. #55952, @gotjosh
* Alerting: Expose info about notification delivery errors in a new /receivers endpoint. #55429, @santihernandezc
* Alerting: Update imported prometheus alertmanager version. #56228, @joeblubaugh
* Alerting: Update imported prometheus alertmanager version. Backport (#56228). #56430, @joeblubaugh
* Alerting: Write and Delete multiple alert instances. #55350, @joeblubaugh
* Core: Implement aria attributes for query rows, improve a11y. #55563, @L-M-K-B
* Custom Branding: Remove custom branding service. (Enterprise)
* Custom branding: Remove UI. (Enterprise)
* DevEnv: Adds docker block for clickhouse. #55702, @owensmallwood
* Docker: removes unneccesary use of edge repo. #54567, @xlson
* Explore: Revert split pane resize feature. #56310, @Elfo404
* Frontend: Make local storage items propagate to different tabs immediately. #55810, @oscarkilhed
* PublicDashboards: Allow disabling an existent public dashboard if it …. #55778, @evictorero
* QueryEditorRow: Only pass error to query editor if panel is not in a loading state. #56350, @kevinwcyu
* Reports: Refresh query variables on time range change. (Enterprise)
* XYChart: Beta release. #55973, @mdvictor
* [9.2.x] Alerting: Start ticker only when scheduler starts (#56339). #56418, @yuri-tceretian

[1.14.1]
* Update Grafana to 9.2.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.2.1)
* Alerting: Improve notification policies created during migration. #52071, @JacobsonMT
* AzureAD: Add option to force fetch the groups from the Graph API. #56916, @gamab
* AzureAD: Add option to force fetch the groups from the Graph API (#56916). #56947, @gamab
* Docs: Note end of release notes publication. #57013, @gguillotte-grafana
* Inspect: Handle JSON tab crash when the provided object is too big to stringify. #55939, @TsotosA
* TablePanel: Footer now updates values on column filtering. #56354, @mdvictor

[1.14.2]
* Update Grafana to 9.2.2
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.2.2)
* Add CreateTempDir func and use it in publish packages logic (#57171)

[1.14.3]
* Update Grafana to 9.2.3
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.2.3)
* Docs: Add information about DB version support to upgrade guide. #57643, @joeblubaugh
* Footer: Update footer release notes link to Github changelog. #57871, @joshhunt
* Prometheus: Do not drop errors in streaming parser. #57698, @kylebrandt
* Prometheus: Flavor/version configuration. #57554, @gtk-grafana
* Prometheus: Provide label values match parameter API when supported prometheus instance is configured. #57553, @gtk-grafana
* Security: Upgrade x/text to version unaffected by CVE-2022-32149. #57797, @yong-jie-gong

[1.14.4]
* Update Grafana to 9.2.4
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.2.4)
* Access Control: Add an endpoint for setting several managed resource permissions. #57893, @IevaVasiljeva
* Accessibility: Increase Select placeholder contrast to be WCAG AA compliant. #58034, @ashharrison90
* Alerting: Append org ID to alert notification URLs. #57123, @neel1996
* Alerting: Make the Grouped view the default one for Rules. #58271, @VikaCep
* Build: Remove unnecessary alpine package updates. #58005, @DanCech
* Chore: Upgrade Go to 1.19.3. #58052, @sakjur
* Google Cloud Monitoring: Set frame interval to draw null values. #57768, @andresmgot
* Instrumentation: Expose when the binary was built as a gauge. #57951, @bergquist
* Loki: Preserve X-ID-Token header. #57878, @siiimooon
* Search: Reduce requests in folder view. #55876, @mvsousa
* TimeSeries: More thorough detection of negative values for auto-stacking direction. #57863, @leeoniya

[1.14.5]
* Update Grafana to 9.2.5
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.2.5)
* Alerting: Log when alert rule cannot be screenshot to help debugging. #58537, @grobinson-grafana
* Alerting: Suggest previously entered custom labels. #57783, @VikaCep
* Canvas: Improve disabled inline editing UX. #58610, @nmarrs
* Canvas: Improve disabled inline editing UX. #58609
* Chore: Upgrade go-sqlite3 to v1.14.16. #58581, @sakjur
* Plugins: Ensure CallResource responses contain valid Content-Type header. #58506, @xnyo
* Prometheus: Handle errors and warnings in buffered client. #58657, @itsmylife
* Prometheus: Upgrade HTTP client library to v1.13.1. #58363, @marefr
* Alerting: Fix screenshots were not cached. #58493, @grobinson-grafana
* Canvas: Fix setting icon from field data. #58499, @nmarrs
* Plugins: Fix don't set Content-Type header if status is 204 for call resource. #50780, @sd2k

[1.14.6]
* Update Grafana to 9.2.6
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.2.6)

[1.15.0]
* Update Grafana to 9.3.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.3.0)
* Fix XSS in runbook URL (#681)

[1.15.1]
* Update Grafana to 9.3.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.3.1)
* Connections: Update "Your connections/Data sources" page. #58589, @mikkancso
* Accessibility: Increase badge constrast to be WCAG AA compliant. #59531, @eledobleefe

[1.15.2]
* Update Grafana to 9.3.2
* Update Cloudron base image to 4.0.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.3.2)
* Traces: Fix for multiple `$_tags` in trace to metrics (#59641)

[1.15.3]
* Update Grafana to 9.3.6
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.3.6)

[1.15.4]
* Update Grafana to 9.3.8
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.3.8)

[1.16.0]
* Update Grafana to 9.4.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.4.1)

[1.16.1]
* Update Grafana to 9.4.2
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.4.2)
* Alerting: Fix boolean default in migration from false to 0. #63952, @alexmobo

[1.16.2]
* Update Grafana to 9.4.3
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.4.3)
* [v9.4.x] NPM: Stop using the folder path before the name path (#64071)
* NPM: Stop using the folder path before the name path (#63851)
* Stop using the folder path before the name path

[1.16.3]
* Update Grafan to 9.4.7
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.4.7)

[1.16.4]
* Update Grafana to 9.4.9
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.4.9)

[1.17.0]
* Update Grafana to 9.5.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.5.1)
* [Highlights](https://grafana.com/docs/grafana/latest/whatsnew/whats-new-in-v9-5/)

[1.17.1]
* Update Grafana to 9.5.2
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.5.2)
* [v9.5.x] Chore: Upgrade Go to 1.20.4. #67757, @papagian
* DataLinks: Encoded URL fixed. #67291, @juanicabanas
* [v9.5.x] Explore: Update table min height (#67321). #67332, @adrapereira

[1.17.2]
* Update Grafana to 9.5.5
* [Changelog](https://github.com/grafana/grafana/releases/tag/v9.5.5)

[1.18.0]
* Update Grafana to 10.0.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.0.1)
* Alerting: Update alerting module to 20230524181453-a8e75e4dfdda. #69011, @yuri-tceretian
* Caching: Update labels for cache insertions counter. (Enterprise)
* Schema: Improve Dashboard kind docs and remove deprecated props. #69652, @ivanortegaalba

[1.18.1]
* Update Grafana to 10.0.3
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.0.3)
* Alerting: Fix edit / view of webhook contact point when no authorization is set. #71972, @gillesdemey
* AzureMonitor: Set timespan in Logs Portal URL link. #71910, @aangelisc
* Plugins: Only configure plugin proxy transport once. #71742, @wbrowne
* Elasticsearch: Fix multiple max depth flatten of multi-level objects. #71636, @fridgepoet
* Elasticsearch: Fix histogram colors in backend mode. #71447, @gabor
* Alerting: Fix state in expressions footer. #71443, @soniaAguilarPeiron
* AppChromeService: Fixes update to breadcrumb parent URL. #71418, @torkelo
* Elasticsearch: Fix using multiple indexes with comma separated string. #71322, @gabor
* Alerting: Fix Alertmanager change detection for receivers with secure settings. #71320, @JacobsonMT
* Transformations: Fix extractFields throwing Error if one value is undefined or null. #71267, @svennergr
* XYChart: Point size editor should reflect correct default (5). #71229, @Develer
* Annotations: Fix database lock while updating annotations. #71207, @sakjur
* TimePicker: Fix issue with previous fiscal quarter not parsing correctly. #71093, @ashharrison90
* AzureMonitor: Correctly build multi-resource queries for Application Insights components. #71039, @aangelisc
* AzureMonitor: Fix metric names for multi-resources. #70994, @asimpson
* Logs: Do not insert log-line into log-fields in json download. #70954, @gabor
* Loki: Fix wrong query expression with inline comments. #70948, @svennergr
* Alerting: Sort NumberCaptureValues in EvaluationString. #71931, @grobinson-grafana
* Alerting: No longer silence paused alerts during legacy migration. #71761, @JacobsonMT
* Auth: Add support for custom signing keys in auth.azure_ad. #71708, @Jguer

[1.18.2]
* Fix typo in `root_url`

[1.18.3]
* Update Grafana to 10.0.4
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.0.4)
* Usage stats: Tune collector execution startup and interval. #72789, @papagian
* Prometheus: Add `present_over_time` syntax highlighting. #72368, @arnaudlemaignen
* Alerting: Improve performance of matching captures. #71999, @grobinson-grafana

[1.19.0]
* Update Grafana to 10.1.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.1.0)

[1.19.1]
* Update Grafana to 10.1.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.1.1)

[1.19.2]
* Update Grafana to 10.1.2
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.1.2)
* Chore: Upgrade Alpine base image to 3.18.3. #74993, @zerok
* Chore: Upgrade Go to 1.20.8. #74980, @zerok

[1.20.0]
* Update Cloudron base image to 4.2.0
* Move to OIDC login

[1.20.1]
* Update Grafana to 10.1.4
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.1.4)

[1.20.2]
* Update Grafana to 10.1.5
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.1.5)
* Chore: Upgrade Go to 1.20.10. #76355, @zerok
* Cloudwatch: Backport 73524 Bring Back Legacy Log Group Picker. #75031, @sarahzinger
* Cloudwatch: Prevent log group requests with ARNs if feature flag is off. #75691, @sarahzinger
* Alerting: Add support for keep_firing_for field from external rulers. #75257, @rwwiv
* Canvas: Avoid conflicting stylesheets when loading SVG icons. #75032, @adela-almasan
* Alerting: Prevent showing "Permissions denied" alert when not accurate. #74925, @VikaCep
* BrowseDashboards: Only remember the most recent expanded folder. #74809, @joshhunt
* Tempo Service Map: Fix context menu links in service map when namespace is present. #74796, @javiruiz01
* Logs Panel: Performance issue while scrolling within panel in safari. #74747, @gtk-grafana
* Bug: Allow to uninstall a deprecated plugin. #74704, @andresmgot
* Licensing: Pass func to update env variables when starting plugin. #74678, @leandro-deveikis
* Nested folders: Fix folder hierarchy in folder responses. #74580, @papagian
* Share link: Use panel relative time for direct link rendered image. #74518, @Clarity-89
* Alerting: Do not exit if Redis ping fails when using redis-based Alertmanager clustering. #74399, @alexweav
* Alerting: Refactor AlertRuleForm and fix annotations step description for cloud rules. #74193, @soniaAguilarPeiron
* RBAC: Chore fix hasPermissionInOrg. (Enterprise)
* Licensing: Updated grpc plugin factory newPlugin signature. (Enterprise)
* Reporting: Add support for old dashboard schema. (Enterprise)

[1.21.0]
* Update Grafana to 10.2.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.2.0)

[1.21.1]
* Update Grafana to 10.2.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.2.1)
* Dashboards: Fix dashboard listing when user can't list any folders. #77988, @IevaVasiljeva
* Search: Modify query for better performance. #77713, @papagian
* RBAC: Allow scoping access to root level dashboards. #77608, @IevaVasiljeva
* CloudWatch Logs: Add labels to alert and expression queries. #77594, @iwysiu
* Bug Fix: Respect data source version when provisioning. #77542, @andresmgot
* Explore: Fix support for angular based datasource editors. #77505, @Elfo404
* Plugins: Fix status_source always being "plugin" in plugin request logs. #77436, @xnyo

[1.21.2]
* Update Grafana to 10.2.2
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.2.2)
* FeatureToggle: Disable dashgpt by default and mark it as preview. #78349, @ivanortegaalba
* SaveDashboardPrompt: Reduce time to open drawer when many changes applied. #78308, @ivanortegaalba
* Alerting: Fix export with modifications URL when mounted on subpath. #78217, @gillesdemey
* Explore: Fix queries (cached & non) count in usage insights. #78216, @Elfo404

[1.21.3]
* Update Grafana to 10.2.3
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.2.3)
* Auth: Improve groups claim setup docs for AzureAD. #79227, @mgyongyosi
* Alerting: Attempt to retry retryable errors. #79175, @gotjosh
* Unified Alerting: Set max_attempts to 1 by default. #79103, @gotjosh
* Auth: Add anonymous users view and stats. #78965, @Jguer
* Alerting: Fix deleting rules in a folder with matching UID in another organization. #79011, @papagian
* CloudWatch: Correctly quote metric names with special characters. #78975, @iwysiu
* DeleteDashboard: Redirect to home after deleting a dashboard. #78936, @ivanortegaalba
* Alerting: Fixes combination of multiple predicates for rule search. #78912, @gillesdemey
* CloudWatch: Fetch Dimension keys correctly from Dimension Picker. #78831, @iwysiu
* Tempo: Fix read-only access error. #78801, @fabrizio-grafana
* Bug: Fix broken ui components when angular is disabled. #78670, @jackw
* InfluxDB: Parse data for table view to have parity with frontend parser. #78551, @itsmylife
* Elasticsearch: Fix processing of raw_data with not-recognized time format. #78380, @ivanahuckova

[1.22.0]
* Update Grafana to 10.3.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.3.0)

[1.22.1]
* Update Grafana to 10.3.4
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.3.4)
* LDAP: Fix LDAP users authenticated via auth proxy not being able to use LDAP active sync. #83750, @Jguer
* Tempo: Add template variable interpolation for filters (#83213). #83706, @joey-grafana
* Elasticsearch: Fix adhoc filters not applied in frontend mode. #83596, @svennergr
* Dashboards: Fixes issue where panels would not refresh if time range updated while in panel view mode. #83525, @kaydelaney
* Auth: Fix email verification bypass when using basic authentication. #83484
* AuthProxy: Invalidate previous cached item for user when changes are made to any header. #83203, @klesh
* LibraryPanels/RBAC: Fix issue where folder scopes weren't being correctly inherited. #82902, @kaydelaney
* LibraryPanels: Fix issue with repeated library panels. #82259, @kaydelaney
* Plugins: Don't auto prepend app sub url to plugin asset paths. #82147, @wbrowne
* Elasticsearch: Set middlewares from Grafana's httpClientProvider. #81929, @svennergr
* Folders: Fix failure to update folder in SQLite. #81862, @papagian
* Loki/Elastic: Assert queryfix value to always be string. #81463, @svennergr

[1.22.2]
* Update Grafana to 10.3.5
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.3.5)
* Alerting: Marshal incoming json.RawMessage in diff (#84692)

[1.23.0]
* Update Grafana to 10.4.2
* [Changelog](https://github.com/grafana/grafana/releases/tag/v10.4.2)
* Angular deprecation: Prefer local "angularDetected" value to the remote one. #85631, @xnyo
* AuthProxy: Fix missing session for ldap auth proxy users. #85237, @Jguer
* Alerting: Fix receiver inheritance when provisioning a notification policy. #85192, @julienduchesne
* CloudMonitoring: Only run query if filters are complete. #85016, @aangelisc

[1.24.0]
* Update Grafana to 11.0.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v11.0.0)
* Alerting: Add two sets of provisioning actions for rules and notifications . #87572, @yuri-tceretian
* Chore: Upgrade go to 1.21.10. #87472, @stephaniehingtgen
* Auth: Force lowercase login/email for users. #86985, @eleijonmarck
* Navigation: Add a return to previous button when navigating to different sections. #86797, @eledobleefe
* DashboardScene: Move add library panel view from grid item to drawer. #86409, @torkelo
* CloudWatch : Add missing AWS/ES metrics. #86271, @thepalbi
* Alerting: Reduce set of fields that could trigger alert state change. #86266, @benoittgt
* OAuth: Make sub claim required for generic oauth behind feature toggle. #86118, @kalleep
* Grafana E2E: Add deprecation notice and update docs. #85778, @sunker
* Loki: Remove API restrictions on resource calls. #85201, @svennergr
* Chore: Upgrade go to 1.21.10. (Enterprise)

[1.25.0]
* Update Grafana to 11.1.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v11.1.0)
* Tracing: Enable traces to profiles. #88896, @marefr
* Auth: Add org to role mappings support to Google integration. #88891, @kalleep
* Alerting: Support AWS SNS integration in Grafana. #88867, @yuri-tceretian
* Auth: Add org to role mappings support to Okta integration. #88770, @mgyongyosi
* Auth: Add org to role mappings support to Gitlab integration. #88751, @kalleep
* Cloudwatch: Use the metric map from grafana-aws-sdk. #88733, @iwysiu
* Alerting: Add option to use Redis in cluster mode for Alerting HA. #88696, @fayzal-g

[1.25.1]
* Update Grafana to 11.1.1
* [Changelog](https://github.com/grafana/grafana/releases/tag/v11.1.1)

[1.25.2]
* Update Grafana to 11.1.3
* [Changelog](https://github.com/grafana/grafana/releases/tag/v11.1.3)

[1.25.3]
* Update Grafana to 11.1.4
* [Changelog](https://github.com/grafana/grafana/releases/tag/v11.1.4)

[1.26.0]
* Update Grafana to 11.2.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v11.2.0)

[1.26.1]
* Update Grafana to v11.2.2
* [Changelog](https://github.com/grafana/grafana/releases/tag/v11.2.2)
* Update pkg/services/cloudmigration/cloudmigrationimpl/xorm_store_test.go

[1.26.2]
* Update Grafana to 11.2.2+security-01
* Fixes CVE-2024-9264

[1.26.3]
* Update Grafana to v11.2.3
* [Changelog](https://github.com/grafana/grafana/releases/tag/v11.2.3)
* Alerting: Fix incorrect permission on POST external rule groups endpoint [CVE-2024-8118] #​93947, @​alexweav
* AzureMonitor: Fix App Insights portal URL for multi-resource trace queries #​94475, @​aangelisc
* Canvas: Allow API calls to grafana origin #​94129, @​adela-almasan
* Folders: Correctly show new folder button under root folder #​94712, @​IevaVasiljeva
* OrgSync: Do not set default Organization for a user to a non-existent Organization #​94549, @​mgyongyosi
* Plugins: Skip install errors if dependency plugin already exists #​94717, @​wbrowne
* ServerSideExpressions: Disable SQL Expressions to prevent RCE and LFI vulnerability #​94959, @​samjewell

[1.27.0]
* Update Grafana to v11.3.0
* [Changelog](https://github.com/grafana/grafana/releases/tag/v11.3.0)

[1.27.1]
* Update grafana to 11.3.1
* [Full Changelog](https://github.com/grafana/grafana/releases/tag/v11.3.1)
* **Alerting:** Make context deadline on AlertNG service startup configurable [#&#8203;96135](https://github.com/grafana/grafana/pull/96135), [@&#8203;fayzal-g](https://github.com/fayzal-g)
* **MigrationAssistant:** Restrict dashboards, folders and datasources by the org id of the signed in user [#&#8203;96345](https://github.com/grafana/grafana/pull/96345), [@&#8203;leandro-deveikis](https://github.com/leandro-deveikis)
* **User:** Check SignedInUser OrgID in RevokeInvite [#&#8203;95490](https://github.com/grafana/grafana/pull/95490), [@&#8203;mgyongyosi](https://github.com/mgyongyosi)
* **Alerting:** Fix escaping of silence matchers in utf8 mode [#&#8203;95347](https://github.com/grafana/grafana/pull/95347), [@&#8203;tomratcliffe](https://github.com/tomratcliffe)
* **Alerting:** Fix overflow for long receiver names [#&#8203;95133](https://github.com/grafana/grafana/pull/95133), [@&#8203;gillesdemey](https://github.com/gillesdemey)

[1.27.2]
* Update grafana to 11.3.2
* [Full Changelog](https://github.com/grafana/grafana/releases/tag/v11.3.2)
* **Backport:** Announcement Banners: Enable feature for all cloud tiers (Enterprise)
* **Fix:** Do not fetch Orgs if the user is authenticated by apikey/sa or render key [#&#8203;97262](https://github.com/grafana/grafana/pull/97262), [@&#8203;mgyongyosi](https://github.com/mgyongyosi)

[1.28.0]
* Update grafana to 11.4.0
* [Full Changelog](https://github.com/grafana/grafana/releases/tag/v11.4.0)
* **Cloudwatch:** OpenSearch PPL and SQL support in Logs Insights

[1.28.1]
* checklist added to CloudronManifest
* CLOUDRON_OIDC_PROVIDER_NAME implemented

[1.28.2]
* Update grafana to 11.4.1
* [Full Changelog](https://github.com/grafana/grafana/releases/tag/v11.4.1)
* **Security:** Update to Go 1.23.5 - Backport to v11.4.x [#&#8203;99123](https://github.com/grafana/grafana/pull/99123), [@&#8203;Proximyst](https://github.com/Proximyst)
* **Security:** Update to Go 1.23.5 - Backport to v11.4.x (Enterprise)
* **Alerting:** AlertingQueryRunner should skip descendant nodes of invalid queries [#&#8203;97830](https://github.com/grafana/grafana/pull/97830), [@&#8203;gillesdemey](https://github.com/gillesdemey)
* **Alerting:** Fix alert rules unpausing after moving rule to different folder [#&#8203;97583](https://github.com/grafana/grafana/pull/97583), [@&#8203;santihernandezc](https://github.com/santihernandezc)
* **Alerting:** Fix label escaping in rule export [#&#8203;98649](https://github.com/grafana/grafana/pull/98649), [@&#8203;moustafab](https://github.com/moustafab)
* **Alerting:** Fix slack image uploading to use new api [#&#8203;98066](https://github.com/grafana/grafana/pull/98066), [@&#8203;moustafab](https://github.com/moustafab)
* **Azure/GCM:** Improve error display [#&#8203;97594](https://github.com/grafana/grafana/pull/97594), [@&#8203;aangelisc](https://github.com/aangelisc)
* **Dashboards:** Fix issue where filtered panels would not react to variable changes [#&#8203;98734](https://github.com/grafana/grafana/pull/98734), [@&#8203;oscarkilhed](https://github.com/oscarkilhed)
* **Dashboards:** Fixes issue with panel header showing even when hide time override was enabled [#&#8203;98747](https://github.com/grafana/grafana/pull/98747), [@&#8203;torkelo](https://github.com/torkelo)
* **Dashboards:** Fixes week relative time ranges when weekStart was changed [#&#8203;98269](https://github.com/grafana/grafana/pull/98269), [@&#8203;torkelo](https://github.com/torkelo)
* **Dashboards:** Panel react for `timeFrom` and `timeShift` changes using variables [#&#8203;98659](https://github.com/grafana/grafana/pull/98659), [@&#8203;Sergej-Vlasov](https://github.com/Sergej-Vlasov)
* **DateTimePicker:** Fixes issue with date picker showing invalid date [#&#8203;97971](https://github.com/grafana/grafana/pull/97971), [@&#8203;torkelo](https://github.com/torkelo)
* **Fix:** Add support for datasource variable queries [#&#8203;98119](https://github.com/grafana/grafana/pull/98119), [@&#8203;sunker](https://github.com/sunker)
* **InfluxDB:** Adhoc filters can use template vars as values [#&#8203;98786](https://github.com/grafana/grafana/pull/98786), [@&#8203;bossinc](https://github.com/bossinc)
* **LibraryPanel:** Fallback to panel title if library panel title is not set [#&#8203;99410](https://github.com/grafana/grafana/pull/99410), [@&#8203;ivanortegaalba](https://github.com/ivanortegaalba)
* **Grafana UI:** Re-add react-router-dom as a dependency [#&#8203;98422](https://github.com/grafana/grafana/pull/98422), [@&#8203;leventebalogh](https://github.com/leventebalogh)

[1.29.0]
* Update grafana to 11.5.0
* [Full Changelog](https://github.com/grafana/grafana/releases/tag/v11.5.0)

[1.29.1]
* Update grafana to 11.5.1
* [Full Changelog](https://github.com/grafana/grafana/releases/tag/v11.5.1)
* **CodeEditor:** Fix cursor alignment [#&#8203;99090](https://github.com/grafana/grafana/pull/99090), [@&#8203;ashharrison90](https://github.com/ashharrison90)
* **TransformationFilter**: Include transformation outputs in transformation filtering options: Include transformation outputs in transformation filtering options [#&#8203;98323](https://github.com/grafana/grafana/pull/98323), [@&#8203;Sergej-Vlasov](https://github.com/Sergej-Vlasov)

[1.29.2]
* Update grafana to 11.5.2
* [Full Changelog](https://github.com/grafana/grafana/releases/tag/v11.5.2)
* **Docker:** Use our own glibc 2.40 binaries [#&#8203;99918](https://github.com/grafana/grafana/pull/99918), [@&#8203;DanCech](https://github.com/DanCech)
* **TransformationFilter:** Include transformation outputs in transformation filtering options [#&#8203;99878](https://github.com/grafana/grafana/pull/99878), [@&#8203;Sergej-Vlasov](https://github.com/Sergej-Vlasov)
* **grafana-ui:** Update InlineField error prop type to React.ReactNode [#&#8203;100373](https://github.com/grafana/grafana/pull/100373), [@&#8203;Clarity-89](https://github.com/Clarity-89)
* **Alerting:** Allow specifying uid for new rules added to groups [#&#8203;100450](https://github.com/grafana/grafana/pull/100450), [@&#8203;yuri-tceretian](https://github.com/yuri-tceretian)
* **Alerting:** Allow specifying uid for new rules added to groups [#&#8203;100450](https://github.com/grafana/grafana/pull/100450), [@&#8203;yuri-tceretian](https://github.com/yuri-tceretian)
* **Alerting:** Call RLock() before reading sendAlertsTo map [#&#8203;99880](https://github.com/grafana/grafana/pull/99880), [@&#8203;santihernandezc](https://github.com/santihernandezc)

[1.30.0]
* Update base image to 5.0.0

