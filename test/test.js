#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    const FOLDER_ID = Math.floor((Math.random() * 100) + 1);
    const FOLDER_NAME = 'Cloudron' + FOLDER_ID;

    let browser, app, cloudronName;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');
        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');

        const tmp = execSync(`cloudron exec --app ${app.id} env`).toString().split('\n').find((l) => l.indexOf('CLOUDRON_OIDC_PROVIDER_NAME=') === 0);
        if (tmp) cloudronName = tmp.slice('CLOUDRON_OIDC_PROVIDER_NAME='.length);
    }

    async function login(username, password, skipChangePassword=true) {
        await browser.get('https://' + app.fqdn + '/user/login');
        await browser.wait(until.elementLocated(By.name('user')), TEST_TIMEOUT);
        await browser.findElement(By.name('user')).sendKeys(username);
        await browser.findElement(By.name('password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[@data-testid="data-testid Login button"]')).click();
        if (skipChangePassword) {
            await waitForElement(By.xpath('//button[@data-testid="data-testid Skip change password button"]'));
            await browser.findElement(By.xpath('//button[@data-testid="data-testid Skip change password button"]')).click();
        }
        await waitForElement(By.xpath('//button[@aria-label="Profile"]'));
    }

    async function loginOIDC(username, password, alreadyAuthenticated=true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/user/login`);
        await browser.sleep(2000);


        await browser.findElement(By.xpath(`//a[contains(., "Sign in with ${cloudronName}")]`)).click();
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await waitForElement(By.xpath('//button[@aria-label="Profile"]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/logout');
        await browser.sleep(3000);
    }

    async function addFolder() {
        await browser.get('https://' + app.fqdn + '/dashboards');
        await waitForElement(By.xpath('//button[.="New"]'));
        await browser.findElement(By.xpath('//button[.="New"]')).click();

        await waitForElement(By.xpath('//button[.="New folder"]'));
        await browser.findElement(By.xpath('//button[.="New folder"]')).click();

        await waitForElement(By.id('folder-name-input'));
        await browser.findElement(By.id('folder-name-input')).sendKeys(FOLDER_NAME);

        await waitForElement(By.xpath('//button[.="Create"]'));
        await browser.findElement(By.xpath('//button[.="Create"]')).click();

        await browser.sleep(2000);
        await waitForElement(By.xpath('//h1[text()="' + FOLDER_NAME + '"]'));
    }

    async function checkFolder() {
        await browser.get('https://' + app.fqdn + '/dashboards');
        await browser.sleep(2000);
        await waitForElement(By.xpath('//a[text()="' + FOLDER_NAME + '"]'));
        await browser.findElement(By.xpath('//a[text()="' + FOLDER_NAME + '"]')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath('//h1[text()="' + FOLDER_NAME + '"]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app (no sso)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, 'admin', 'admin'));
    it('can add folder', addFolder);
    it('can logout', logout);
    it('uninstall app', async function () {
        await browser.get('about:blank'); // avoid NXDOMAIN
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    it('install app (sso)', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, 'admin', 'admin'));
    it('can add folder', addFolder);
    it('can logout', logout);

    it('can oidc login', loginOIDC.bind(null, username, password, false));
    it('can check folder', checkFolder);
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', async function () {
        await browser.get('about:blank'); // avoid NXDOMAIN
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can oidc login', loginOIDC.bind(null, username, password, true));
    it('can check folder', checkFolder);
    it('can logout', logout);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id); });

    it('can oidc login', loginOIDC.bind(null, username, password, true));
    it('can check folder', checkFolder);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank'); // avoid NXDOMAIN
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can oidc login', loginOIDC.bind(null, username, password, true));
    it('can check folder', checkFolder);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank'); // avoid NXDOMAIN
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync('cloudron install --appstore-id com.grafana.cloudronapp --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, 'admin', 'admin'));
    it('can add folder', addFolder);
    it('can logout', logout);
    it('can oidc login', loginOIDC.bind(null, username, password, true));
    it('can check folder', checkFolder);
    it('can logout', logout);

    it('can update', function () { execSync('cloudron update --app ' + app.id, EXEC_ARGS); });

    it('can oidc login', loginOIDC.bind(null, username, password, true));
    it('can check folder', checkFolder);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank'); // avoid NXDOMAIN
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
